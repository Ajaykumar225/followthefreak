﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target_controller : MonoBehaviour
{
    public static Game_manager.Holes newHole;
    public float St;
    // Use this for initialization
    void Start()
    {
        newHole = Game_manager.Holes.noSide;
    }


    // Update is called once per frame
    void Update()
    {
        // InvokeRepeating("CheckForSleep", 0.1f, 0.1f);
        if (Mathf.Abs(GetComponent<Rigidbody>().velocity.x) == 0 && Mathf.Abs(GetComponent<Rigidbody>().velocity.z) == 0
            && !Game_manager.instance.IsPocketed
            && Game_manager.instance.GoalNum != 0)
            Game_manager.instance.StreakEnd();

    }
    void CheckForSleep()
    {
        if (Mathf.Abs(GetComponent<Rigidbody>().velocity.x) < St && Mathf.Abs(GetComponent<Rigidbody>().velocity.z) < St)
        {
            GetComponent<Rigidbody>().Sleep();
            //Debug.Log(GetComponent<Rigidbody>().velocity);
            CancelInvoke("CheckForSleep");
        }
    }
    public void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "cue")
        {
            Game_manager.instance.IsCueTargetCollided = true;
            Game_manager.instance.IsPocketed = false;
            InvokeRepeating("CheckForSleep", St, St);

        }
        if (collision.gameObject.tag == "Wall1")
        {
            Cue_controller.newWall = Game_manager.Walls.Left;
            Game_manager.instance.Count++;

        }
        if (collision.gameObject.tag == "Wall2")
        {
            Cue_controller.newWall = Game_manager.Walls.Bottom;
            Game_manager.instance.Count++;
        }
        if (collision.gameObject.tag == "Wall3")
        {
            Cue_controller.newWall = Game_manager.Walls.Right;
            Game_manager.instance.Count++;
        }

        if (collision.gameObject.tag == "Wall4")
        {
            Cue_controller.newWall = Game_manager.Walls.Top;
            Game_manager.instance.Count++;
        }


    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Hole1")
            newHole = Game_manager.Holes.TopLeft;

        if (other.gameObject.tag == "Hole2")
            newHole = Game_manager.Holes.BottomLeft;
        if (other.gameObject.tag == "Hole3")
            newHole = Game_manager.Holes.TopRight;
        if (other.gameObject.tag == "Hole4")
            newHole = Game_manager.Holes.BottomRight;
        Game_manager.instance.IsPocketed = true;
        Game_manager.instance.GoalNum++;
        Game_manager.instance.OnGoal();
        Game_manager.instance.InvokeRepeating("BallInsta", 0.5f, 0.5f);
        Game_manager.instance.CheckForStreak();
    }

}
