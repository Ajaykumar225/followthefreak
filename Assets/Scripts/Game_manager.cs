﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Game_manager : MonoBehaviour
{
    public static Game_manager instance;
    public GameObject Cue_ball;
    public GameObject Target_ball;
    public Vector3 position;
    public bool IsPocketed;
    public int Count;
    public int GoalNum;
    public Text Streak;
    public Canvas Retry;
    public Text clearshot;
    public enum Walls
    {
        Top,
        Bottom,
        Left,
        Right,
        nothing
    }
    public enum Holes
    {
        TopLeft,
        TopRight,
        BottomLeft,
        BottomRight, noSide
    }
    public bool IsCueTargetCollided;
    public Text Insaneshot;
    // Use this for initialization
    void Start()
    {
        GoalNum = 0;
        //WhichWall = 0;
        //WhichPocket = 0;
        Count = 0;
        IsPocketed = false;
        Insaneshot.enabled = false;
        Streak.enabled = false;
        clearshot.enabled = false;
        // Retry.enabled = false;
    }
    private void Awake()
    {
        if (instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
    // Update is called once per frame
    void Update()
    {



    }
    public void OnGoal()
    {
        Debug.Log(Target_controller.newHole);
        Debug.Log(Cue_controller.newWall);
        IsPocketed = true;
        Target_ball.SetActive(false);
        Debug.Log(Count);
        if (Count == 0)
        {
            clearshot.enabled = true;
        }
        if ((Cue_controller.newWall == Walls.Left && ((Target_controller.newHole == Holes.TopRight) || (Target_controller.newHole == Holes.BottomRight)))
                || (Cue_controller.newWall == Walls.Right && ((Target_controller.newHole == Holes.TopLeft) || (Target_controller.newHole == Holes.BottomLeft)))
                || (Cue_controller.newWall == Walls.Top && ((Target_controller.newHole == Holes.BottomRight) || (Target_controller.newHole == Holes.BottomLeft)))
                || (Cue_controller.newWall == Walls.Bottom && ((Target_controller.newHole == Holes.TopLeft) || (Target_controller.newHole == Holes.TopRight))))
        {
            Debug.Log("insane");
            Insaneshot.text = "InsaneShot!";
            Insaneshot.enabled = true;
        }
        else
        {
            Cue_controller.newWall = Walls.nothing;
            Target_controller.newHole = Holes.noSide;
        }
    }

    public void BallInsta()
    {
        if (Mathf.Abs(Cue_ball.GetComponent<Rigidbody>().velocity.x) == 0f && Mathf.Abs(Cue_ball.GetComponent<Rigidbody>().velocity.z) == 0f)
        {
            Insaneshot.enabled = false;
            clearshot.enabled = false;
            position = new Vector3(Random.Range(-4.62f, 3.6f), 0.415f, Random.Range(0.86f, 5.83f));
            Target_ball.transform.position = position;
            Target_ball.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
            Target_ball.SetActive(true);
            CancelInvoke("BallInsta");
        }

    }
    public void OnFoul()
    {

        Cue_ball.SetActive(false);
        Retry.enabled = true;

        if (Mathf.Abs(Target_ball.GetComponent<Rigidbody>().velocity.x) == 0f && Mathf.Abs(Target_ball.GetComponent<Rigidbody>().velocity.z) == 0f)
        {
            // position = new Vector3(Random.Range(-4.62f, 3.6f), 0.415f, Random.Range(0.86f, 5.83f));


            //Cue_ball.transform.position = position;

            // Cue_ball.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
            //Cue_ball.SetActive(true);
            // Count = 0;
            
            CancelInvoke("OnFoul");


        }
    }
    public void CheckForStreak()
    {
        if (IsPocketed)
        {
            Streak.text = "Streak:" + GoalNum;
            Streak.enabled = true;
            Invoke("OffText", 1f);
        }
     
    }
    public void OffText()
    {
        Streak.enabled = false; ;
    }
    public void StreakEnd()
    {
        Streak.text = "StreakEnded";
        Streak.enabled = true;
        GoalNum = 0;
        Invoke("OffText", 1f);
    }
}

