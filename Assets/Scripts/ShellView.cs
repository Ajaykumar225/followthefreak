﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Khadga.Collision.Gameplay
{
    public class ShellView : MonoBehaviour
    {

        public float ForceMultiplier;
        public float MaxDragLength;
        private float Length = 0;
        private Vector3 DragResultant;
        private Vector3 DragStartposition;
        private Vector3 DragEndposition;
        private Vector3 CurrentDragPosition;
        bool IsDragInProgress;
        LineRenderer LineRenderer;
        public GameObject cb;
        public float SleepTime;

        void CheckForSleep()
        {
            if (Mathf.Abs(GetComponent<Rigidbody>().velocity.x) < SleepTime && Mathf.Abs(GetComponent<Rigidbody>().velocity.z) < SleepTime)
            {
                GetComponent<Rigidbody>().Sleep();
                //Debug.Log(GetComponent<Rigidbody>().velocity);
                Game_manager.instance.Count = 0;
                Game_manager.instance.IsCueTargetCollided = false;
                
                Cue_controller.newWall = Game_manager.Walls.nothing;
                Target_controller.newHole = Game_manager.Holes.noSide;
                CancelInvoke("CheckForSleep");
            }
        }
        public void Start()
        {
            LineRenderer = GetComponent<LineRenderer>();
        }



        void ShowDragLength()
        {
        }

        private void Update()
        {
            if (IsDragInProgress)
            {
                LineRenderer.enabled = true;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 50f))
                {
                    CurrentDragPosition = hit.point;
                }
                else
                {
                    CurrentDragPosition = ray.GetPoint(0);
                }
                LineRenderer.SetPosition(1, CurrentDragPosition);
            }
            else
            {
                LineRenderer.enabled = false;
            }
        }

        private void OnMouseDown()
        {
            DragResultant = Vector3.zero;
            Length = 0;
            Vector2 touchDeltaPosition = Input.mousePosition;
            IsDragInProgress = true;
            DragStartposition = new Vector3(touchDeltaPosition.x, 0, touchDeltaPosition.y);
            LineRenderer.SetPosition(0, new Vector3(cb.transform.position.x, 0.1f, cb.transform.position.z));
        }

        private void OnMouseUp()
        {
            Vector2 touchPosition = Input.mousePosition;
            DragEndposition = new Vector3(touchPosition.x, 0, touchPosition.y);
            DragResultant = DragEndposition - DragStartposition;
            Length = DragResultant.magnitude;
            if (Mathf.Abs(GetComponent<Rigidbody>().velocity.x) == 0 && Mathf.Abs(GetComponent<Rigidbody>().velocity.z) == 0)

                GetComponent<Rigidbody>().AddForce(ForceMultiplier * -GetForce(Length) * (DragResultant.normalized));
            InvokeRepeating("CheckForSleep", SleepTime, SleepTime);
            CancelInvoke("ShowDragLength");
            IsDragInProgress = false;
        }

        float GetForce(float length)
        {
            if (length < MaxDragLength)
            {
                return length;
            }
            else
                return MaxDragLength;
        }


    }
}
