﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cue_controller : MonoBehaviour
{

    public static Game_manager.Walls newWall;
    // Use this for initialization
    void Start()
    {

        newWall = Game_manager.Walls.nothing;
    }


    // Update is called once per frame
    void Update()
    {



    }
    public void OnCollisionEnter(Collision collision)
    {
        if (!Game_manager.instance.IsCueTargetCollided)
        {
            if (collision.gameObject.tag == "Wall1")
            {
                newWall = Game_manager.Walls.Left;

            }
            if (collision.gameObject.tag == "Wall2")
            {
                newWall = Game_manager.Walls.Bottom;

            }
            if (collision.gameObject.tag == "Wall3")
            {
                newWall = Game_manager.Walls.Right;

            }

            if (collision.gameObject.tag == "Wall4")
            {
                newWall = Game_manager.Walls.Top;

            }
        }
    }
    public void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Hole1" || collision.gameObject.tag == "Hole2" || collision.gameObject.tag == "Hole3" || collision.gameObject.tag == "Hole4")
        {
            Game_manager.instance.InvokeRepeating("OnFoul", 0.05f, 0.05f);
           
        }

    }

}
